sumOfMultiples :: Int -> Int
sumOfMultiples n = sum [ x | x <- [1..(n-1)], x `mod' 3 == 0 || x `mod` 5 == 0]

main = do
       print(sumOfMultiples 1000)