ones = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
        "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]

tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

toEnglish n
	| 0 <= n && n < 20 = ones !! n
	| 20 <= n && n < 100 = (tens !! (div n 10)) ++ (if (mod n 10 /= 0) then (ones !! (mod n 10)) else "")
	| 100 <= n && n < 1000 = (ones !! (div n 100)) ++ "hundred"
		++ (if (mod n 100 /= 0) then ("and" ++ toEnglish (mod n 100)) else "")
	| 1000 <= n && n < 1000000 = toEnglish (div n 1000) ++ "thousand"
		++ (if (mod n 1000 /= 0) then (toEnglish (div n 1000)) else "")

numberOfLetters = sum [length (toEnglish n) | n <- [1..1000]]

main = do
       print (numberOfLetters)