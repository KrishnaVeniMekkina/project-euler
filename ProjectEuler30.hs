digitFifthPowers = sum [i | i <- [2..10^6 - 1], i == fifthPowerDigitSum i]

fifthPowerDigitSum 0 = 0
fifthPowerDigitSum n = (mod n 10)^5 + (fifthPowerDigitSum (div n 10))

main = do
       print (digitFifthPowers)