fibs :: [Int]
fibs = 1 : 2 : zipWith (+) fibs (tail fibs)

evenSum :: Int -> Int
evenSum n = sum $ filter even $ takeWhile (< n) fibs

main = do
       print(evenSum 4000000)