(defn isPrime [n]
  (empty? (filter #(= 0 (mod n %)) (range 2 n))))

(defn primeFactors [n]
  (apply max (filter (isPrime ())))

(println (primeFactors 600851475143))