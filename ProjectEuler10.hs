isPrime n = length [ x | x <- [2..n], n `mod` x == 0] == 1
sumOfPrimesBelow n = sum $ filter isPrime [ x | x <- [1..n]]
main = do
    print $ sumOfPrimesBelow 2000000
