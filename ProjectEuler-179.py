def factors(num):
  return list(i for i in range(1,num+1) if num % i == 0)

def factor_range(LIMIT):
  return len(set((i,j) for i in range(LIMIT) for j in range(LIMIT) if len(factors(i)) == len(factors(j)) and i - j == 1))
