def distinct_powers():
  return set(pow(a,b) for a in range(2,101) for b in range(2,101))
