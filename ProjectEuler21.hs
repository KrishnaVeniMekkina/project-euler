amicable :: Int -> Bool
amicable n = let m = divisorSum n in (m /= n) && (divisorSum m) == n

divisorSum :: Int -> Int
divisorSum n = sum [k | k <- [1..n-1], (mod n k) == 0]

sumOfAmicableNumbers = sum [n | n <- [1..10^4], amicable n]

main = do
       print (show sumOfAmicableNumbers)