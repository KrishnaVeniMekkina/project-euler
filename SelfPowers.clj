(defn power [n]
  (reduce * (repeat n n)))

(defn selfPowers [n]
  (apply + (map power(range 1 (inc n)))))

(println (selfPowers 1000))