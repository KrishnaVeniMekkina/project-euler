(defn square [n]
  (* n n))

(defn difference_squareSum_sumSquare [n]
  (- (square (apply + (range 1 (inc n)))) (apply + (map square (range 1 (inc n))))))

(println (difference_squareSum_sumSquare 100))