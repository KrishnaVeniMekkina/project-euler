(let [nums (range 1 101)
      sumsqr (apply + (map #(* % %) nums))
      sqrsum (#(* % %) (apply + nums))]
  (- sqrsum sumsqr))