import Data.List 
import Data.Char
todigits n = map digitToInt $ show n
factors n = [i | i <- [2..n], n `mod` i == 0]
isPrime n = ((length (factors n)) == 1)
isPermutation n x = (((sort(todigits n)) ==  (sort(todigits x))))
isPrimePermutation n x = (isPrime x) && (isPermutation n x)

main = do
    print $ (isPrimePermutation 1234 4321)

