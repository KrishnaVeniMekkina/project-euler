def is_palindrome(n:int) -> bool:
    if str(n) == str(n) == str(n)[::-1]:
        return True
def Palindrome():
    return max(list(i*j for i in range(1000,1,-1) for j in range(1000,1,-1) if is_palindrome(i*j)))
print(Palindrome())
