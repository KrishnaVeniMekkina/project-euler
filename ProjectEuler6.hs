diff n = squareOfSum - sumOfSquares
    where squareOfSum = (sum [1..n]) ^ 2
          sumOfSquares = sum $ map (^2) [1..n]

main = do
       print $ diff 100