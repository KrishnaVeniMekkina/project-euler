def sum_of_digit_squares():
  return sum(set(pow(i,j) for i,j in zip(range(100),range(100))))
