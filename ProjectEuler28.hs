size = 1001  -- Must be odd
main = putStrLn (show ans)
ans = 1 + sum [4 * n * n - 6 * (n - 1) | n <- [3, 5 .. size]]