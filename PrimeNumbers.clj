(defn isPrime [n]
  (empty? (filter #(= 0 (mod n %)) (range 2 n))))

(defn primeNums [n]
 (count (map isPrime (range 2 (inc n))))
  )

(println (primeNums 12))


